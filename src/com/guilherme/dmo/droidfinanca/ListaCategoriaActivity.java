package com.guilherme.dmo.droidfinanca;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.guilherme.dmo.droidfinanca.adapter.CategoriaAdapter;
import com.guilherme.dmo.droidfinanca.model.Categoria;
import com.guilherme.dmo.droidfinanca.persistence.DataBase;

public class ListaCategoriaActivity extends Activity {

	private ListView listCategorias;
	private DataBase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lista_activity);

		db = (DataBase) getIntent().getParcelableExtra("database");
		List<Categoria> list = db.selectAllCategoria();
		
		listCategorias = (ListView) findViewById(R.id.listLancamentos);
		listCategorias.setAdapter(new CategoriaAdapter(this, list));
		listCategorias.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				CategoriaAdapter adapter = (CategoriaAdapter) arg0.getAdapter();
				Categoria c = (Categoria) adapter.getItem(arg2);
				
				//Chamando a tela para altera��o/exclus�o
				Bundle params = new Bundle();
				params.putParcelable("database", db);
				params.putParcelable("categoria", c);
				params.putString("op", "alterar");
				Intent it = new Intent(getBaseContext(), ManterCategoriaActivity.class);
				it.putExtras(params);
				startActivityForResult(it, 1);
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == 1) {
			if(resultCode == RESULT_OK) {
				db = (DataBase) data.getParcelableExtra("database");
				listCategorias.invalidate();
				listCategorias.setAdapter(new CategoriaAdapter(this, db.selectAllCategoria()));
				
				Bundle params = new Bundle();
				params.putParcelable("database", db);
				setResult(RESULT_OK, new Intent().putExtras(params));
			}
		}
	}
}
