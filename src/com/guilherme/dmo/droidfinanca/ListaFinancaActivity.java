package com.guilherme.dmo.droidfinanca;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.guilherme.dmo.droidfinanca.adapter.LancamentoAdapter;
import com.guilherme.dmo.droidfinanca.model.Lancamento;
import com.guilherme.dmo.droidfinanca.persistence.DataBase;

public class ListaFinancaActivity extends Activity {

	private ListView listLancamentos;
	private DataBase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lista_activity);

		db = (DataBase) getIntent().getParcelableExtra("database");
		List<Lancamento> list = db.selectAllLancamento();
		
		listLancamentos = (ListView) findViewById(R.id.listLancamentos);
		listLancamentos.setAdapter(new LancamentoAdapter(this, list));
		listLancamentos.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				LancamentoAdapter adapter = (LancamentoAdapter) arg0.getAdapter();
				Lancamento l = (Lancamento) adapter.getItem(arg2);
			
				Bundle params = new Bundle();
				params.putParcelable("database", db);
				params.putParcelable("lancamento", l);
				params.putString("op", "alterar");
				Intent it = new Intent(getBaseContext(), ManterFinancaActivity.class);
				it.putExtras(params);
				startActivityForResult(it, 1);
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == 1) {
			if(resultCode == RESULT_OK) {
				db = (DataBase) data.getParcelableExtra("database");
				listLancamentos.invalidate();
				listLancamentos.setAdapter(new LancamentoAdapter(this, db.selectAllLancamento()));
				
				Bundle params = new Bundle();
				params.putParcelable("database", db);
				setResult(RESULT_OK, new Intent().putExtras(params));
			}
		}
	}
}
