package com.guilherme.dmo.droidfinanca;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.guilherme.dmo.droidfinanca.graph.PieGraph;
import com.guilherme.dmo.droidfinanca.persistence.DataBase;

public class MainActivity extends Activity {

	private DataBase db;
	private ListView listAcoes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);

		db = new DataBase();

		listAcoes = (ListView) findViewById(R.id.listAcoes);
		listAcoes.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, new String[] {
				"Manter Categorias", "Manter Cr�ditos/D�bitos",
		"Listar Cr�ditos/D�bitos", "Listar Categorias", "Por no gr�fico"}));

		listAcoes.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view,
					int arg2, long position) {

				Bundle params = new Bundle();
				params.putParcelable("database", db);
				params.putString("op", "novo");
				Intent it;

				switch ((int) position) {
				case 0:
					it = new Intent(getBaseContext(),
							ManterCategoriaActivity.class);
					it.putExtras(params);
					startActivityForResult(it, 0);
					break;
				case 1:
					it = new Intent(getBaseContext(),
							ManterFinancaActivity.class);
					it.putExtras(params);
					startActivityForResult(it, 1);
					break;
				case 2:
					it = new Intent(getBaseContext(), ListaFinancaActivity.class);
					it.putExtras(params);
					startActivityForResult(it, 2);
					break;
				case 3:
					it = new Intent(getBaseContext(), ListaCategoriaActivity.class);
					it.putExtras(params);
					startActivityForResult(it, 3);
					break;
				case 4:
					PieGraph pie = new PieGraph();
					Intent i = pie.getIntent(getBaseContext(), db.selectCountCategorias());
					startActivity(i);
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			db = data.getParcelableExtra("database");
		}
	}
}
