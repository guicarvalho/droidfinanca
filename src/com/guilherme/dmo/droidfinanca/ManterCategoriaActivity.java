package com.guilherme.dmo.droidfinanca;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.guilherme.dmo.droidfinanca.model.Categoria;
import com.guilherme.dmo.droidfinanca.persistence.DataBase;

public class ManterCategoriaActivity extends Activity {

	private EditText editDescCategoria;

	private Button buttonCadastrar;
	private Button buttonExcluir;

	private String op;

	private DataBase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manter_categorias);

		Intent it = getIntent();
		op = it.getExtras().getString("op");
		db = (DataBase) it.getParcelableExtra("database");

		editDescCategoria = (EditText) findViewById(R.id.editDescCategoria);
		buttonCadastrar = (Button) findViewById(R.id.buttonCadastrar);
		buttonExcluir = (Button) findViewById(R.id.buttonExcluir);

		if (op.equals("novo")) {
			buttonExcluir.setEnabled(false);
			buttonCadastrar.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					cadastrarCategoria();
				}
			});
		} else if (op.equals("alterar")){
			final Categoria c = getIntent().getParcelableExtra("categoria");
			editDescCategoria.setText(c.getDescricao());

			buttonExcluir.setEnabled(true);
			buttonExcluir.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					db.delete(c);
					Toast.makeText(getBaseContext(), "Excluido com sucesso!",
							Toast.LENGTH_SHORT).show();
					Bundle params = new Bundle();
					params.putParcelable("database", db);
					setResult(RESULT_OK, new Intent().putExtras(params));
				}
			});

			buttonCadastrar.setText("Alterar");
			buttonCadastrar.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					c.setDescricao(editDescCategoria.getText().toString());
					db.update(c);
					Toast.makeText(getBaseContext(), "Atualizado com sucesso!",
							Toast.LENGTH_SHORT).show();
					Bundle params = new Bundle();
					params.putParcelable("database", db);
					setResult(RESULT_OK, new Intent().putExtras(params));
				}
			});
		}
	}

	private void cadastrarCategoria() {
		String descricao = editDescCategoria.getText().toString();
		db.insert(new Categoria(descricao));
		Toast.makeText(getBaseContext(), "Categoria cadastrada com sucesso!",
				Toast.LENGTH_SHORT).show();

		Bundle params = new Bundle();
		params.putParcelable("database", db);
		setResult(RESULT_OK, new Intent().putExtras(params));
	}
}
