package com.guilherme.dmo.droidfinanca;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.guilherme.dmo.droidfinanca.model.Categoria;
import com.guilherme.dmo.droidfinanca.model.Lancamento;
import com.guilherme.dmo.droidfinanca.persistence.DataBase;

public class ManterFinancaActivity extends Activity {

	private Spinner spinnerCategoria;

	private EditText editDescricao;
	private EditText editData;
	private EditText editValor;

	private RadioGroup radioGrupo;
	private RadioButton radioCredito;
	private RadioButton radioDebito;

	private Button buttonCadastrar;
	private Button buttonRemover;

	private DataBase db;

	private String op; // armazena o tipo de opera��o solicitada pelo usu�rio

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manter_financas);

		Intent it = getIntent();
		op = it.getExtras().getString("op");
		db = (DataBase) it.getParcelableExtra("database");

		ArrayAdapter<Categoria> adapter = new ArrayAdapter<Categoria>(this,
				android.R.layout.simple_spinner_item, preencherCategorias());
		spinnerCategoria = (Spinner) findViewById(R.id.spinnerCategoria);
		spinnerCategoria.setAdapter(adapter);

		editDescricao = (EditText) findViewById(R.id.editDescricao);
		editData = (EditText) findViewById(R.id.editData);
		editValor = (EditText) findViewById(R.id.editValor);
		radioGrupo = (RadioGroup) findViewById(R.id.radioGroupTipo);
		radioCredito = (RadioButton) findViewById(R.id.radioCredito);
		radioDebito = (RadioButton) findViewById(R.id.radioDebito);
		buttonCadastrar = (Button) findViewById(R.id.buttonCadastrar);
		buttonRemover = (Button) findViewById(R.id.buttonExcluir);

		if (op.equals("novo")) {
			buttonRemover.setEnabled(false);
			buttonCadastrar.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					cadastrarFinanca();
				}
			});
		} else if (op.equals("alterar")) {
			final Lancamento l = getIntent().getParcelableExtra("lancamento");

			editData.setText(l.getData());
			editDescricao.setText(l.getDescricao());
			editValor.setText(l.getValor());
			int pos = db.selectAllCategoria().indexOf(
					db.selectCategoriaById(l.getCategoria().getId()));
			spinnerCategoria.setSelection(pos);
			if (l.getTipo().equals("C")) {
				radioCredito.setChecked(true);
			} else {
				radioDebito.setChecked(true);
			}

			buttonRemover.setEnabled(true);
			buttonRemover.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					db.delete(l);
					Toast.makeText(getBaseContext(), "Excluido com sucesso!",
							Toast.LENGTH_SHORT).show();
					Bundle params = new Bundle();
					params.putParcelable("database", db);
					setResult(RESULT_OK, new Intent().putExtras(params));
				}
			});

			buttonCadastrar.setText("Alterar");
			buttonCadastrar.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					l.setCategoria((Categoria) spinnerCategoria
							.getSelectedItem());
					l.setData(editData.getText().toString());
					l.setDescricao(editDescricao.getText().toString());
					l.setValor(editValor.getText().toString());
					l.setTipo(radioGrupo.getCheckedRadioButtonId() == R.id.radioCredito ? "C"
							: "D");
					db.update(l);
					Toast.makeText(getBaseContext(), "Atualizado com sucesso!",
							Toast.LENGTH_SHORT).show();
					Bundle params = new Bundle();
					params.putParcelable("database", db);
					setResult(RESULT_OK, new Intent().putExtras(params));
				}
			});
		}
	}

	private void cadastrarFinanca() {
		String descricao = editDescricao.getText().toString();
		String data = editData.getText().toString();
		String valor = editValor.getText().toString();
		String tipo = radioGrupo.getCheckedRadioButtonId() == R.id.radioCredito ? "C"
				: "D";
		Categoria categoria = (Categoria) spinnerCategoria.getSelectedItem();

		db.insert(new Lancamento(descricao, data, valor, tipo, categoria));
		Toast.makeText(getBaseContext(), "Finan�a cadastrada com sucesso!",
				Toast.LENGTH_SHORT).show();

		Bundle params = new Bundle();
		params.putParcelable("database", db);
		setResult(RESULT_OK, new Intent().putExtras(params));
	}

	private List<Categoria> preencherCategorias() {
		List<Categoria> categorias = new ArrayList<Categoria>();

		for (Categoria c : db.selectAllCategoria()) {
			categorias.add(c);
		}

		return categorias;
	}
}
