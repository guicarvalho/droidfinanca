package com.guilherme.dmo.droidfinanca.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.guilherme.dmo.droidfinanca.R;
import com.guilherme.dmo.droidfinanca.model.Categoria;

public class CategoriaAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<Categoria> categorias;

	public CategoriaAdapter(Activity context, List<Categoria> categorias) {
		//this.context = context;
		this.categorias = categorias;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return categorias != null ? categorias.size() : 0;
	}

	@Override
	public Object getItem(int index) {
		return categorias != null ? categorias.get(index) : null;
	}

	@Override
	public long getItemId(int index) {
		return index;
	}

	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		ViewHolder holder = null;
		if(view == null) {
			holder = new ViewHolder();
			int layout = R.layout.categoria_item;
			view = inflater.inflate(layout, null);
			view.setTag(holder);
			holder.textCategoria = (TextView) view.findViewById(R.id.textCategoriaDesc);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		
		Categoria c = categorias.get(pos);

		holder.textCategoria.setText(c.getDescricao());
		
		return view;
	}

	static class ViewHolder {
		TextView textCategoria;
	}
}
