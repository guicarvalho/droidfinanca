package com.guilherme.dmo.droidfinanca.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.guilherme.dmo.droidfinanca.R;
import com.guilherme.dmo.droidfinanca.model.Lancamento;

public class LancamentoAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<Lancamento> lancamentos;

	public LancamentoAdapter(Activity context, List<Lancamento> lancamentos) {
		//this.context = context;
		this.lancamentos = lancamentos;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return lancamentos != null ? lancamentos.size() : 0;
	}

	@Override
	public Object getItem(int index) {
		return lancamentos != null ? lancamentos.get(index) : null;
	}

	@Override
	public long getItemId(int index) {
		return index;
	}

	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		ViewHolder holder = null;
		if(view == null) {
			holder = new ViewHolder();
			int layout = R.layout.lancamento_item;
			view = inflater.inflate(layout, null);
			view.setTag(holder);
			holder.textCategoria = (TextView) view.findViewById(R.id.textCategoria);
			holder.textData = (TextView) view.findViewById(R.id.textData);
			holder.textValor = (TextView) view.findViewById(R.id.textValor);
			holder.textTipo = (TextView) view.findViewById(R.id.textTipo);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		
		Lancamento l = lancamentos.get(pos);

		holder.textCategoria.setText(l.getCategoria().getDescricao());
		holder.textData.setText(l.getData());
		holder.textValor.setText("R$ " + l.getValor().replace('.', ','));
		holder.textTipo.setText(l.getTipo().equals("C")? "Cr�dito" : "D�bito");
		holder.textTipo.setTextColor(l.getTipo().equals("C")? Color.GREEN : Color.RED);
		
		return view;
	}

	static class ViewHolder {
		TextView textCategoria;
		TextView textValor;
		TextView textData;
		TextView textTipo;
	}
}
