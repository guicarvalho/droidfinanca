package com.guilherme.dmo.droidfinanca.graph;

import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

public class PieGraph {
	public Intent getIntent(Context context, Map<String, Integer> valores) {

		CategorySeries series = new CategorySeries("Categorias");
		for (String categoria : valores.keySet()) {
			series.add(categoria, valores.get(categoria));
		}

		int[] cores = new int[] { Color.BLACK, Color.BLUE, Color.CYAN,
				Color.GRAY, Color.GREEN, Color.MAGENTA, Color.RED, Color.WHITE,
				Color.YELLOW };

		DefaultRenderer renderer = new DefaultRenderer();
		int k = 0;
		for(int i=0; i < valores.size(); i++) {
			SimpleSeriesRenderer r = new SimpleSeriesRenderer();
			r.setColor(cores[k]);
			renderer.addSeriesRenderer(r);
			
			k = i > cores.length ? 0 : i+1; 
		}
		
		Intent intent = ChartFactory.getPieChartIntent(context, series, renderer, "Gr�fico de categorias");
		
		return intent;
	}
}
