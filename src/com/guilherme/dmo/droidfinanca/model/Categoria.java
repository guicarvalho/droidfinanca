package com.guilherme.dmo.droidfinanca.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Categoria implements Parcelable {
	private static long valorGeradoAutomaticamente = 0L;
	private String descricao;
	private long id; 

	public Categoria(String descricao) {
		this.descricao = descricao;
		this.id = valorGeradoAutomaticamente++;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(descricao);
		dest.writeLong(id);
	}

	public static final Parcelable.Creator<Categoria> CREATOR = new Creator<Categoria>() {

		@Override
		public Categoria[] newArray(int size) {
			return new Categoria[size];
		}

		@Override
		public Categoria createFromParcel(Parcel source) {
			return new Categoria(source);
		}
	};

	private Categoria(Parcel in) {
		descricao = in.readString();
		id = in.readLong();
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
