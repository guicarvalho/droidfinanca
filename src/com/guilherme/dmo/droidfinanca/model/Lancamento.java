package com.guilherme.dmo.droidfinanca.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Lancamento implements Parcelable {
	private static long valorGeradoAutomaticamente = 0L;
	private long id;
	private String descricao;
	private String data;
	private String valor;
	private String tipo;
	private Categoria categoria;

	public Lancamento(String descricao, String data, String valor, String tipo,
			Categoria categoria) {
		this.descricao = descricao;
		this.data = data;
		this.valor = valor;
		this.tipo = tipo;
		this.categoria = categoria;
		this.id = valorGeradoAutomaticamente++;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(descricao);
		dest.writeString(data);
		dest.writeString(valor);
		dest.writeString(tipo);
		dest.writeValue(categoria);
	}

	public static final Parcelable.Creator<Lancamento> CREATOR = new Creator<Lancamento>() {

		@Override
		public Lancamento[] newArray(int size) {
			return new Lancamento[size];
		}

		@Override
		public Lancamento createFromParcel(Parcel source) {
			return new Lancamento(source);
		}
	};

	private Lancamento(Parcel in) {
		id = in.readLong();
		descricao = in.readString();
		data = in.readString();
		valor = in.readString();
		tipo = in.readString();
		categoria = (Categoria) in.readValue(Categoria.class.getClassLoader());
	}
}
