package com.guilherme.dmo.droidfinanca.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;

import com.guilherme.dmo.droidfinanca.model.Categoria;
import com.guilherme.dmo.droidfinanca.model.Lancamento;

public class DataBase implements Parcelable {

	private List<Categoria> tableCategoria;
	private List<Lancamento> tableLancamento;

	public DataBase() {
		tableCategoria = new ArrayList<Categoria>();
		tableLancamento = new ArrayList<Lancamento>();
	}

	public void insert(Categoria c) {
		tableCategoria.add(c);
	}

	public void update(Categoria c) {
		int row = tableCategoria.indexOf(selectCategoriaById(c.getId()));
		tableCategoria.set(row, c);
	}

	public void delete(Categoria c) {
		tableCategoria.remove(selectCategoriaById(c.getId()));
	}

	public List<Categoria> selectAllCategoria() {
		return tableCategoria;
	}

	public Categoria selectCategoriaById(long id) {
		for (Categoria item : tableCategoria) {
			if (item.getId() == id) {
				return item;
			}
		}
		return null;
	}

	public void insert(Lancamento l) {
		tableLancamento.add(l);
	}

	public void update(Lancamento l) {
		int row = tableLancamento.indexOf(selectLancamentoById(l.getId()));
		tableLancamento.set(row, l);
	}

	public void delete(Lancamento l) {
		tableLancamento.remove(selectLancamentoById(l.getId()));
	}

	public List<Lancamento> selectAllLancamento() {
		Collections.sort(tableLancamento, new Comparator<Lancamento>() {

			@Override
			public int compare(Lancamento l1, Lancamento l2) {
				return l1.getData().compareTo(l2.getData());
			}
		});
		
		return tableLancamento;
	}

	public Lancamento selectLancamentoById(long id) {
		for (Lancamento item : tableLancamento) {
			if (item.getId() == id) {
				return item;
			}
		}
		return null;
	}

	public Map<String, Integer> selectCountCategorias() {
		Map<String, Integer> totais = new HashMap<String, Integer>();

		for(Categoria c : selectAllCategoria()) {
			totais.put(c.getDescricao(), 0);
		}

		for(Lancamento l : selectAllLancamento()) {
			if(totais.containsKey(l.getCategoria().getDescricao())) {
				int total = totais.get(l.getCategoria().getDescricao()) + 1;
				totais.put(l.getCategoria().getDescricao(), total);
			}
		}
		
		return totais;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(tableCategoria);
		dest.writeList(tableLancamento);
	}

	public static final Parcelable.Creator<DataBase> CREATOR = new Creator<DataBase>() {

		@Override
		public DataBase[] newArray(int size) {
			return new DataBase[size];
		}

		@Override
		public DataBase createFromParcel(Parcel source) {
			return new DataBase(source);
		}
	};

	private DataBase(Parcel in) {
		tableCategoria = new ArrayList<Categoria>();
		in.readList(tableCategoria, Categoria.class.getClassLoader());
		tableLancamento = new ArrayList<Lancamento>();
		in.readList(tableLancamento, Lancamento.class.getClassLoader());
	}
}
